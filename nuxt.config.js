require('dotenv').config()
const webpack = require('webpack')
module.exports = {
  mode: 'universal',
  /*
   ** Headers of the page
   */
  head: {
    title: 'Innova Scientific | App',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: process.env.npm_package_description || ''
      }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      {
        rel: 'stylesheet',
        href:
          'https://fonts.googleapis.com/css?family=Poppins:400,500,600,700|Baloo|Fira+Sans:400,500,600&display=swap'
      },
      {
        rel: 'stylesheet',
        href:
          'https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap-grid.min.css'
      }
    ],
    bodyAttrs: {
      class: 'hattbo-template'
    },
    htmlAttrs: {
      lang: 'es',
      amp: true
    }
  },
  /*
   ** Customize the progress-bar color
   */
  loading: { color: '#fff' },
  /*
   ** Global CSS
   */
  css: [
    '~/assets/css/main.css',
    'element-ui/lib/theme-chalk/index.css',
    '~/assets/fonts/Noir/stylesheet.css'
  ],
  /*
   ** Plugins to load before mounting the App
   */
  plugins: [
    { src: '@/plugins/element-ui' },
    { src: '@/plugins/vue-friendly-iframe', ssr: false },
    { src: '@/plugins/vue-eva-icons.js', ssr: false },
    { src: '@/plugins/i18n.js', ssr: false },
    { src: '@/plugins/vue-pell-editor', ssr: false },
    { src: '@/plugins/vue-data-tables', ssr: false }
  ],
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [],

  serverMiddleware: ['~/api/db/index', '~/api/auth.js'],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/auth'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL:
      process.env.NODE_ENV === 'development'
        ? process.env.API_URL_DEV
        : process.env.API_URL_PRO
  },

  server: {
    port: process.env.SERVER_PORT // default: 3000
  },
  /*
   ** Build configuration
   */
  build: {
    transpile: [/^element-ui/],
    /*
     ** You can extend webpack config here
     */
    extend(config, ctx) {
      plugins: [
        new webpack.ProvidePlugin({
          _: 'lodash'
          // ...etc.
        })
      ]
    }
  },
  loading: '~/components/loading.vue',

  auth: {
    redirect: {
      logout: '/login',
      callback: '/login',
      home: '/dashboard'
    },
    strategies: {
      local: {
        endpoints: {
          login: {
            baseURL:
              process.env.NODE_ENV == 'development'
                ? process.env.API_URL_DEV
                : process.env.API_URL_PRO,
            url: '/auth/login',
            method: 'post',
            propertyName: 'token.accessToken'
          },
          logout: {
            baseURL:
              process.env.NODE_ENV == 'development'
                ? process.env.API_URL_DEV
                : process.env.API_URL_PRO,
            url: '/auth/logout',
            method: 'post'
          },
          user: {
            baseURL:
              process.env.NODE_ENV == 'development'
                ? process.env.API_URL_DEV
                : process.env.API_URL_PRO,
            url: '/auth/user',
            method: 'get',
            propertyName: 'user'
          }
        }
      }
    }
  }
}
