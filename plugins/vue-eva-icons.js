import Vue from 'vue'
import EvaIcons from 'vue-eva-icons'

export default () => {
  Vue.use(EvaIcons)
}
