var v = {
    init: function(settings) {
        var vars = {};
        for (let elem in settings) {
            vars[elem] = settings[elem];
        }
        v.var = vars;
    },
    onReady: function() {
        v.hover.init();
        v.screnmobile.init();
        v.fullscreen.init();
        v.fullv.init();
    },
    screnmobile: {
        init: () => {
            v.screnmobile.isMobile();
            v.orientation.init();
        },
        isMobile: () => {
            // Listen for orientation changes
            window.addEventListener("orientationchange", function() {
                // Announce the new orientation number
                if (window.orientation == 0) {
                    $("#orientation").text('Portrait');
                } else {
                    $("#orientation").text('Landscape');
                }
            }, false);
            // Listen for resize changes
            window.addEventListener("resize", function() {
                // Get screen size (inner/outerWidth, inner/outerHeight)
            }, false);
            // Find matches
            var mql = window.matchMedia("(orientation: portrait)");
            // If there are matches, we're in portrait
            if (mql.matches) {
                // Portrait orientation
                $("#orientation").text('Portrait');
            } else {
                // Landscape orientation
                $("#orientation").text('Landscape');
            }
            // Add a media query change listener
            mql.addListener(function(m) {
                if (m.matches) {
                    // Changed to portrait
                    $("#orientation").text('Portrait');
                } else {
                    // Changed to landscape
                    $("#orientation").text('Landscape');
                }
            });
        }
    },
    orientation: {
        init: () => {
            v.orientation.change();
        },
        change: () => {
            var _LOCK_BUTTON = document.querySelector("#lock-landscape-button"),
                _UNLOCK_BUTTON = document.querySelector("#unlock-button"),
                _STATUS = document.querySelector("#orientation");
            _STATUS.innerHTML = screen.orientation.type + ' mode';
            // // upon lock to landscape-primary mode
            // document.querySelector("#lock-landscape-button").addEventListener('click', function() {
            //     if (document.documentElement.requestFullscreen) document.querySelector("#uk-s-container").requestFullscreen();
            //     else if (document.documentElement.webkitRequestFullScreen) document.querySelector("#uk-s-container").webkitRequestFullScreen();
            //     // screen.orientation.lock();
            //     screen.orientation.lock("landscape-primary").then(function() {
            //         _LOCK_BUTTON.style.display = 'none';
            //         _UNLOCK_BUTTON.style.display = 'block';
            //     }).catch(function(error) {
            //         alert(error);
            //     });
            // });
            // // upon unlock
            document.querySelector("#unlock-button").addEventListener('click', function() {
                screen.orientation.unlock();
                _LOCK_BUTTON.style.display = 'block';
                _UNLOCK_BUTTON.style.display = 'none';
            });
            // // when screen orientation changes
            // screen.orientation.addEventListener("change", function() {
            //     _STATUS.innerHTML = screen.orientation.type + ' mode';
            // });
            // // on exiting full-screen lock is automatically released
            // document.addEventListener("fullscreenchange", function() {
            //     _LOCK_BUTTON.style.display = 'block';
            //     _UNLOCK_BUTTON.style.display = 'none';
            // });
            // document.addEventListener("webkitfullscreenchange", function() {
            //     _LOCK_BUTTON.style.display = 'block';
            //     _UNLOCK_BUTTON.style.display = 'none';
            // });
        }
    },
    fullscreen: {
        init: () => {
            $(v.var.btn.fullscreen.enable).on('click', function() {
                // $(goto).show();
                v.fullscreen.enable();
            });
            $(v.var.btn.fullscreen.disable).on('click', function() {
                // $(goto).show();
                v.fullscreen.disable();
            });
        },
        enable: () => {
            // if (document.fullscreenElement) return;
            // return document.documentElement.requestFullscreen();
            // screen.orientation.lock();
            // screen.orientation.lock('landscape-primary');
            // screen.orientation.addEventListener("change", function() {
            //     _STATUS.innerHTML = screen.orientation.type + ' mode';
            // });
            const el = document.documentElement;
            if (el.requestFullscreen) {
                el.requestFullscreen();
            } else if (el.msRequestFullscreen) {
                el.msRequestFullscreen();
            } else if (el.mozRequestFullScreen) {
                el.mozRequestFullScreen();
            } else if (el.webkitRequestFullscreen) {
                el.webkitRequestFullscreen();
            }
        },
        disable: () => {
            // const el = document.documentElement;
            
            if (document.exitFullscreen) {
                document.exitFullscreen();
            } else if (document.msExitFullscreen) {
                document.msExitFullscreen();
            } else if (document.mozCancelFullScreen) {
                document.mozCancelFullScreen();
            } else if (document.webkitExitFullscreen) {
                document.webkitExitFullscreen();
            }
            // // document.cancelFullScreen()
            // document.webkitCancelFullScreen();
            // // screen.orientation.unlock();
            // screen.orientation.unlock();
        },
    },
    fullv: {
        init: () => {
            $(v.var.btn.start).on('click', function() {
                $(v.var.cont.start).hide();
                $(v.var.cont.intro).show();
                v.fullv.intro();
            });
            v.fullv.action.init();
        },
        intro: () => {
            v.video.play(v.var.video.id);
            $(v.var.video.id).on('ended', function() {
                $(v.var.cont.intro).hide();
                $(v.var.cont.mainmenu).show();
                v.video.stop(v.var.video.id);
                v.audio.play(v.var.sound.menu);
            });
            $(v.var.btn.omitirntro).on('click', function() {
                $(v.var.cont.intro).hide();
                $(v.var.cont.mainmenu).show();
                v.video.stop(v.var.video.id);
                v.audio.play(v.var.sound.menu);
            });
            // $('.uk-card').mouseover(function() {
            //     v.video.play(v.var.sound.btn);
            // });
            v.fullv.menu();
        },
        menu: () => {
            $(v.var.menu.cont).mouseenter(function() {
                v.audio.play(v.var.sound.btn);
            }).mouseleave(function() {
                v.audio.stop(v.var.sound.btn);
            });
            $(v.var.menu.cont).on('click', function() {
                const goto = $(this).data('to');
                v.fullv.action.shownewvideo(goto);
                // $(goto).show();
            });
            v.fullv.videobtn();
        },
        videobtn: () => {
            $(v.var.btn.goto).on('click', function() {
                const goto = $(this).data('to');
                const left = $(this).data('left');
                console.log('DATA', goto, left);
                v.fullv.action.shownayvideo('.' + goto, '.' + left);
                // $(goto).show();
            });
        },
        action: {
            init: () => {
                v.fullv.action.gohome();
            },
            default: () => {},
            gohome: () => {
                $(v.var.btn.gohome).on('click', function() {
                    const left = $(this).data('left');
                    $(v.var.cont.mainmenu).show();
                    $(v.var.cont.list).hide();
                    const vcont = $('.' + left);
                    vcont.hide();
                    v.video.stop($(vcont).find('video'));
                });
            },
            shownayvideo: (goto, left) => {
                v.video.stop($(left).find('video'));
                v.video.stop('video');
                v.video.play($(goto).find('video'));
                $(left).hide();
                $(goto).show();
            },
            shownewvideo: (id) => {
                $(v.var.cont.mainmenu).hide();
                const vlist = $(v.var.cont.list);
                vlist.show();
                const vcont = vlist.find('.' + id);
                vcont.show();
                v.video.stop('video');
                v.video.play(vcont.find('video'));
            },
        }
    },
    video: {
        play: (id) => {
            $(id).trigger('play');
        },
        pause: (id) => {
            $(id).trigger('pause');
        },
        stop: (id) => {
            $(id).get(0).pause();
            $(id).get(0).currentTime = 0;
        }
    },
    audio: {
        play: (id) => {
            $(id).trigger('play');
            $(id).volume = 0.1;
        },
        pause: (id) => {
            $(id).trigger('pause');
            $(id).volume = 0.1;
        },
        stop: (id) => {
            $(id).get(0).pause();
            $(id).get(0).currentTime = 0;
            $(id).volume = 0.1;
        }
    },
    hover: {
        init: () => {
            v.hover.overlay();
        },
        overlay: () => {
            var test = $(v.var.video.overlay);
            var moveTimer;
            test.on("mouseout", function() {
                $(this).addClass('overhide').removeClass('overshow');
                clearTimeout(moveTimer);
            });
            test.on("mousemove", function() {
                // console.log("I'm moving!");
                clearTimeout(moveTimer);
                moveTimer = setTimeout(function() {
                    // console.log("I stopped moving!");
                    test.addClass('overhide').removeClass('overshow');
                }, 2000)
                $(this).addClass('overshow').removeClass('overhide');
            });
        }
    },
    storage: {
        init: (name, data) => {
            v.storage.push(name, data);
        },
        push: (name, data) => {
            sessionStorage.setItem(name, JSON.stringify(data));
            return true;
        },
        get: (name) => {
            if (sessionStorage.getItem(name)) {
                storage_tmp = JSON.parse(sessionStorage.getItem(name));
                return storage_tmp;
            } else {
                return false;
            }
        },
        clean: () => {
            // $(window).bind('beforeunload', function(e) {
            //     // code to execute when browser is closed
            //     sessionStorage.clear();
            // });
        },
    },
    width: () => {
        return $(window).width();
    }
};
v.init({
    video: {
        id: '#main_video',
        overlay: '.video_overlay',
    },
    cont: {
        start: '#cont_start',
        intro: '#cont_intro',
        mainmenu: '#cont_mainmenu',
        list: '#videos_list'
    },
    btn: {
        start: '#btn_start',
        omitirntro: '#btn_omitir_intro',
        gohome: '.go_home',
        goto: '.btn_go_to',
        fullscreen: {
            enable: '.btn_fullscreen_enable',
            disable: '.btn_fullscreen_disable',
        }
    },
    sound: {
        btn: '#sound_button',
        menu: '#sound_menu',
    },
    menu: {
        cont: '.uk-card-menu'
    }
});
$(v.onReady);