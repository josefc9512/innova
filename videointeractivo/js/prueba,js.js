var v = {
    init: function(settings) {
        var vars = {};
        for (let elem in settings) {
            vars[elem] = settings[elem];
        }
        v.var = vars;
    },
    onReady: function() {
        v.hover.init();
        v.fullv.init();
    },
    hover: {
        init: () => {
            v.hover.overlay();
        },
        overlay: () => {
            $('body').mouseover(function() {
                $(v.video.overlay).addClass('overshow').removeClass('overhide');
            });


            $("body").mouseenter(function() {
                $(v.video.overlay).addClass('overshow').removeClass('overhide');

            }).mouseleave(function() {
                $(v.video.overlay).addClass('overhide').removeClass('overshow');
            });
        }
    },
    fullv: {
        init: () => {
            // sessionStorage.clear();
            if (!v.storage.get('video_full')) {
                if (v.width() > 900) {
                    v.fullv.reproducir(v.var.video.name.desk);
                } else {
                    v.fullv.reproducir(v.var.video.name.mobile);
                }
                v.storage.init('video_full', 'iniciado');
            } else {
                $(v.var.video.parent).hide();
                $('body').removeClass('videofullplaying');
                console.log('ya reproduciste el intro en este navegador');
            }
        },
        reproducir: (videoname) => {
            v.fullv.onlyonetime();
            $(v.var.video.parent).show();
            $('#' + videoname).show();
            $('body').addClass('videofullplaying');
            
            var mivideo = document.getElementById(videoname);
            mivideo.play();
            v.fullv.saltar();
            mivideo.onended = function() {
                mivideo.pause();
                $(v.var.video.parent).fadeOut();
                $('body').removeClass('videofullplaying');
                console.log('Video finalizado...');
            }
        },
        onEnded: () => {
            console.log('video.. finished')
        },
        saltar: () => {
            $(v.var.video.saltar).click(function() {
                $(v.var.video.parent).fadeOut();
                $('body').removeClass('videofullplaying');
            });
            $(v.var.video.salir).click(function() {
                $(v.var.video.parent).fadeOut();
                $('body').removeClass('videofullplaying');
            });
        },
        onlyonetime: () => {
            v.storage.clean();
        }
    },
    storage: {
        init: (name, data) => {
            v.storage.push(name, data);
        },
        push: (name, data) => {
            sessionStorage.setItem(name, JSON.stringify(data));
            return true;
        },
        get: (name) => {
            if (sessionStorage.getItem(name)) {
                storage_tmp = JSON.parse(sessionStorage.getItem(name));
                return storage_tmp;
            } else {
                return false;
            }
        },
        clean: () => {
            // $(window).bind('beforeunload', function(e) {
            //     // code to execute when browser is closed
            //     sessionStorage.clear();
            // });
        },
    },
    width: () => {
        return $(window).width();
    }
};
v.init({
    video: {
        parent: '.fullscreen-bg',
        overlay: '#video_overlay',
        id: {
            desk: '#video_full_desktop',
            mobile: '#video_full_movile',
        },
        name: {
            desk: 'video_full_desktop',
            mobile: 'video_full_movile',
        },
        saltar: '#saltar_video',
        salir: '.salir_video'
    }
});
$(v.onReady);