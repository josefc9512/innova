'use strict'
const Sequelize = require('sequelize')
require("dotenv").config();

const sequelize = new Sequelize(
  process.env.DATABASE_NAME,
  process.env.DATABASE_USER,
  process.env.DATAUSER_PASSWORD,
  {
    host: process.env.DATABASE_IP,
    dialect: 'mysql',
    timezone: '-05:00',
    dialectOptions: {
      dateStrings: true,
      typeCast: true
    }
  }
)

sequelize
  .authenticate()
  .then(() => {
    console.log('Se ha conectado correctamente con la base de datos')
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err)
  })

const db = {}

db.Sequelize = Sequelize
db.sequelize = sequelize

db.users = require('../models/users')(sequelize, Sequelize)

db.files = require('../models/files')(sequelize, Sequelize)
db.searchs = require('../models/searchs')(sequelize, Sequelize)
db.databases = require('../models/databases')(sequelize, Sequelize)
db.services = require('../models/services')(sequelize, Sequelize)
db.user_services = require('../models/user_services')(sequelize, Sequelize)

//Relations
db.files.hasMany(db.searchs, { foreignKey: 'file_id' })
db.searchs.belongsTo(db.files, { foreignKey: 'file_id' })

db.files.hasMany(db.databases, { foreignKey: 'file_id' })
db.databases.belongsTo(db.files, { foreignKey: 'file_id' })

db.files.hasMany(db.services, { foreignKey: 'file_id' })
db.services.belongsTo(db.files, { foreignKey: 'file_id' })

db.files.hasMany(db.users, { foreignKey: 'picture' })
db.users.belongsTo(db.files, { foreignKey: 'picture' })


db.users.hasMany(db.user_services, { foreignKey: 'user_id' })
db.user_services.belongsTo(db.users, { foreignKey: 'user_id' })

db.services.hasMany(db.user_services, { foreignKey: 'service_id' })
db.user_services.belongsTo(db.services, { foreignKey: 'service_id' })

module.exports = db
