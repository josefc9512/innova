'use strict'

module.exports = (sequelize, DataTypes) => {
  const searchs = sequelize.define(
    'searchs',
    {
      name: {
        type: DataTypes.STRING,
        required: true
      },
      file_id: {
        type: DataTypes.INTEGER,
        required: true
      },
      url: {
        type: DataTypes.STRING,
        required: true
      },
      status: {
        type: DataTypes.INTEGER,
        required: true
      },
    },
    {
      freezeTableName: false,
      tableName: 'searchs'
    }
  )
  return searchs
}
