'use strict'

module.exports = (sequelize, DataTypes) => {
  const files = sequelize.define(
    'files',
    {
      name: {
        type: DataTypes.STRING,
        required: true
      },
      type: {
        type: DataTypes.STRING,
        required: true
      },
      size: {
        type: DataTypes.DOUBLE,
        required: true
      },
      path: {
        type: DataTypes.STRING,
        required: true
      }
    },
    {
      freezeTableName: false,
      tableName: 'files'
    }
  )
  return files
}
