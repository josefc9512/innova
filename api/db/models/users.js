'use strict'

module.exports = (sequelize, DataTypes) => {
  const users = sequelize.define(
    'users',
    {
      names: {
        type: DataTypes.STRING,
        required: true
      },
      email: {
        type: DataTypes.STRING,
        required: true
      },
      password: {
        type: DataTypes.STRING,
        required: true
      },
      phone: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      picture: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      role: {
        type: DataTypes.STRING,
        required: true
      },
      type_login: {
        type: DataTypes.STRING,
        allowNull: true
      },
      status: {
        type: DataTypes.INTEGER,
        required: true
      }
    },
    {
      freezeTableName: false,
      tableName: 'users'
    }
  )
  return users
}
