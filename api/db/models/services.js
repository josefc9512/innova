'use strict'

module.exports = (sequelize, DataTypes) => {
  const services = sequelize.define(
    'services',
    {
      name: {
        type: DataTypes.STRING,
        required: true
      },
      file_id: {
        type: DataTypes.INTEGER,
        required: true
      },
      url: {
        type: DataTypes.STRING,
        required: true
      },
      description: {
        type: DataTypes.STRING,
        required: true
      },
      image_alt: {
        type: DataTypes.STRING,
        required: true
      },
      status: {
        type: DataTypes.INTEGER,
        required: true
      },
    },
    {
      freezeTableName: false,
      tableName: 'services'
    }
  )
  return services
}
