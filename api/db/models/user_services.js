'use strict'

module.exports = (sequelize, DataTypes) => {
  const user_services = sequelize.define(
    'user_services',
    {
      user_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      service_id: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      start_date: {
        type: DataTypes.STRING,
        allowNull: true,
        required: false
      },
      end_date: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      avalible: {
        type: DataTypes.INTEGER,
        allowNull: true,
        required: false
      },
      status: {
        type: DataTypes.INTEGER,
        required: true
      }
    },
    {
      freezeTableName: false,
      tableName: 'user_services'
    }
  )
  return user_services
}
