'use strict'

const express = require('express')
const bodyParser = require('body-parser')
const morgan = require('morgan')
const app = express()
const jwt = require('express-jwt')
const cors = require('cors')

require('dotenv').config()

app.use(morgan('combined'))
app.use(
  bodyParser.urlencoded({
    limit: '250mb',
    extended: true,
    parameterLimit: 50000
  })
)
app.use(bodyParser.json({ limit: '250mb', extended: true }))
app.use(cors())

const TOKEN_ROUTE = process.env.TOKEN_ROUTE

// JWT middleware
app.use(
  jwt({
    secret: TOKEN_ROUTE
  }).unless({
    path: [
      '/api/users/login',
      '/api/users/sendEmail',
      '/api/login',
      '/api/register',
      '/api/loginfacebook',
      '/api/searchs',
      '/api/databases',
      '/api/services',
      '/api/files/upload/picture/base64',
      '/api/users/update/photo',
      '/api/users/resetpassapp'
    ]
  })
)

var users = require('./routes/users')
var login = require('./routes/login')
var searchs = require('./routes/searchs')
var services = require('./routes/services')
var files = require('./routes/files')
var dashboard = require('./routes/dashboard')
var databases = require('./routes/databases')
var user_services = require('./routes/user_services')

//

app.use('/users', users)
app.use('/', login)
app.use('/files', files)
app.use('/searchs', searchs)
app.use('/services', services)
app.use('/databases', databases)
app.use('/dashboard', dashboard)
app.use('/user_services', user_services)

module.exports = {
  path: '/api',
  handler: app
}
