// Users.js
module.exports = (function() {
  'use strict'
  //   const router = require('express').Router()
  const express = require('express')

  const bodyParser = require('body-parser')
  const db = require('../config/config')
  const fileUpload = require('express-fileupload')
  require('dotenv').config()

  const app = express()

  app.use(bodyParser.json())

  app.get('/', async (req, res) => {
    let users = await getUsers()
    let services = await getService()
    let searchs = await getSearch()
    res.json({
      users: users,
      services: services,
      searchs: searchs
    })
  })

  function getUsers() {
    return db.users
      .count({
        where: {
          status: 1
        }
      })
      .then(res => {
        return res
      })
      .catch(err => {
        return false
      })
  }

  function getService() {
    return db.services
      .count({
        where: {
          status: 1
        }
      })
      .then(res => {
        return res
      })
      .catch(err => {
        return false
      })
  }

  function getSearch() {
    return db.searchs
      .count({
        where: {
          status: 1
        }
      })
      .then(res => {
        return res
      })
      .catch(err => {
        return false
      })
  }

  return app
})()
