// Users.js
module.exports = (function() {
    'use strict'
    const app = require('express').Router()
    const db = require('../config/config')
    const bcrypt = require('bcrypt')
    require('dotenv').config()
    const jwt = require('jsonwebtoken')

    const TOKEN_ROUTE = process.env.TOKEN_ROUTE

    // app.post('/', (req, res) => {
    //   const body = req.body

    //   const { names, password, name, email } = body

    //   let password_hash = bcrypt.hashSync(password, 10)

    //   db.users
    //     .create({
    //       names: names,
    //       name: name,
    //       password: password_hash,
    //       email: email
    //     })
    //     .then(user => {
    //       res.json(user)
    //     })
    //     .catch(err => {
    //       res.json(err)
    //     })
    // })

    app.post('/login', async(req, res) => {
        if (Object.keys(req.body).length != 0) {
            const body = req.body
            const { email, password } = body

            if (email && password) {
                // let countUser = await ifExistUser(email).then(count => {
                //   return count
                // })

                let user = await getUser(email).then(result => {
                    console.log(result)
                    return result
                })

                if (!user)
                    return res
                        .status(404)
                        .json({ status: 404, error: true, message: 'El usuario no existe' })

                const result = bcrypt.compareSync(password, user.password)

                if (!result)
                    return res.status(401).json({
                        status: 401,
                        error: true,
                        message: 'El password no coincide'
                    })

                // const accessToken = jwt.sign({ id: user.id }, TOKEN_ROUTE)
                const accessToken = jwt.sign({
                        id: user.id,
                        email: user.email,
                        picture: user.file ? user.file.path : '/logo-inovs.png',
                        names: user.names,
                        phone: user.phone
                    },
                    TOKEN_ROUTE
                )

                res.status(200).json({
                        token: accessToken,
                        services_user: user.user_services
                    })
                    // res.status(200).json({
                    //   status: 200,
                    //   error: false,
                    //   message: 'Bienvenido',
                    //   user: user,
                    //   access_token: accessToken,
                    //   expires_in: 'Nothing'
                    // })
            } else {
                res.status(203).json({
                    status: 203,
                    error: true,
                    message: 'Hubo un error a la hora de realizar tu consulta'
                })
            }
        } else {
            res.status(203).json({
                status: 203,
                error: true,
                message: 'Hay campos vacios'
            })
        }
    })

    app.post('/loginfacebook', async(req, res) => {
        if (Object.keys(req.body).length != 0) {
            const body = req.body
            const { email, name, type_login } = body

            if ((email && name, type_login)) {
                // let countUser = await ifExistUser(email).then(count => {
                //   return count
                // })

                let user = await getUser(email).then(result => {
                    return result
                })

                if (!user) {
                    let password_hash = bcrypt.hashSync(makeid(10), 10)

                    db.users
                        .create({
                            names: name,
                            password: password_hash,
                            email: email,
                            type_login: type_login
                        })
                        .then(user => {
                            const accessToken = jwt.sign({ id: user.id }, TOKEN_ROUTE)

                            return res.json({
                                user: user,
                                error: false,
                                message: 'Bienvenido',
                                token: accessToken,
                                expires_in: 'Nothing'
                            })
                        })
                        .catch(err => {
                            res.json(err)
                        })
                } else {
                    console.log(user.status)
                    const id = user.id
                    db.users
                        .update({
                            status: 1
                        }, {
                            where: {
                                id: id
                            }
                        })
                        .then(project => {
                            res.json(project)
                        })
                }

                const accessToken = jwt.sign({ id: user.id, email: user.email },
                    TOKEN_ROUTE
                )

                res.status(200).json({
                    token: accessToken,
                    services_user: user.user_services
                })
            } else {
                res.status(203).json({
                    status: 203,
                    error: true,
                    message: 'Hubo un error a la hora de realizar tu consulta'
                })
            }
        } else {
            res.status(203).json({
                status: 203,
                error: true,
                message: 'Hay campos vacios'
            })
        }
    })

    app.post('/register', async(req, res) => {
        const body = req.body

        const { name, password, email } = body

        let password_hash = bcrypt.hashSync(password, 10)

        let countUser = await ifExistUser(email).then(count => {
            return count
        })

        if (countUser)
            return res.status(203).json({
                status: 203,
                error: true,
                message: 'Este correo ya esta registrado'
            })

        db.users
            .create({
                names: name,
                password: password_hash,
                email: email
            })
            .then(user => {
                const accessToken = jwt.sign({ id: user.id }, TOKEN_ROUTE)

                res.json({
                    user: user,
                    error: false,
                    message: 'Bienvenido',
                    access_token: accessToken,
                    expires_in: 'Nothing'
                })
            })
            .catch(err => {
                res.json(err)
            })
    })

    app.get('/', (req, res) => {
        res.json('hola')
    })

    function makeid(length) {
        var result = ''
        var characters =
            'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        var charactersLength = characters.length
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength))
        }
        return result
    }

    function getUser(email) {
        return db.users.findOne({
            include: [{
                    model: db.files
                },
                {
                    model: db.user_services,
                    required: false,
                    include: [{
                        model: db.services,
                        required: false
                    }],
                    where: {
                        status: 1
                    }
                }
            ],
            where: {
                email: email
            }
        })
    }

    function ifExistUser(email) {
        return db.users.count({
            where: {
                email: email
            },
            include: []
        })
    }

    return app
})()