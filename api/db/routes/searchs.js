// searchs.js
module.exports = (function() {
  'use strict'
  const app = require('express').Router()
  const db = require('../config/config')
  require('dotenv').config()

  app.post('/', (req, res) => {
    const body = req.body

    db.searchs
      .create(body)
      .then(search => {
        res.json(search)
      })
      .catch(err => {
        res.json(err)
      })
  })

  app.get('/', (req, res) => {
    db.searchs
      .findAll({
        where: {
          status: 1
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(searchs => {
        res.json(searchs)
      })
  })

  app.get('/:id', (req, res) => {
    const id = req.params.id
    if (!id)
      return res.status(404).json({
        error: true,
        message: 'No se puede realizar la consulta por falta de datos'
      })

    db.searchs
      .findOne({
        where: {
          id: id
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(searchs => {
        res.json(searchs)
      })
  })

  app.put('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.searchs
      .update(update, {
        where: {
          id: id
        }
      })
      .then(project => {
        res.json(project)
      })
  })

  app.delete('/:id', (req, res) => {
    const id = req.params.id
    db.searchs
      .update(
        {
          status: 0
        },
        {
          where: {
            id: id
          }
        }
      )
      .then(project => {
        res.json(project)
      })
  })

  return app
})()
