// Users.js
module.exports = (function() {
  'use strict'
  //   const router = require('express').Router()
  const express = require('express')

  const bodyParser = require('body-parser')
  const db = require('../config/config')
  const fileUpload = require('express-fileupload')
  require('dotenv').config()
  const mime = require('mime')
  var fs = require('fs')
  const jsonwebtoken = require('jsonwebtoken')
  const app = express()

  app.use(bodyParser.json())
  app.use(
    fileUpload({
      limits: { fileSize: 250 * 1024 * 1024 }
    })
  )

  app.use(bodyParser.json({ limit: '250mb', extended: true }))

  app.use(
    bodyParser.urlencoded({
      limit: '250mb',
      extended: true,
      parameterLimit: 50000
    })
  )

  app.post('/upload', async (req, res) => {
    if (Object.keys(req.files).length == 0) {
      res.status(400).send('No files were uploaded.')
    }
    let file = req.files.file

    let name_file = file.name
    let size_file = file.size
    let type_temp = file.mimetype

    let a = type_temp.split('/')

    let type = a[1]

    let name_path = randomString(50)

    let path_file = '/assets/upload/images/' + name_path + '.' + type

    file.mv('static' + path_file, err => {
      if (err) return res.status(500).send(err)

      let data = {
        name: name_file,
        type: type,
        size: size_file,
        path: path_file
      }

      db.files
        .create(data)
        .then(file => {
          let f = file
          res.json(f)
        })
        .catch(err =>
          res.json({
            code: 'error_create',
            message: 'El archivo no pudo ser subido al servido'
          })
        )
    })
  })

  app.post('/upload/picture/base64', (req, res) => {
    if (req.body.constructor === Object && Object.keys(req.body).length == 0)
      res.status(400).send('No files were uploaded.')

    let pictureBase = req.body.picture

    var decodedImg = decodeBase64Image(pictureBase)
    var imageBuffer = decodedImg.data
    var type = decodedImg.type
    var extension = mime.getExtension(type)
    let date_n = randomString(50)
    var fileName = date_n + '.' + extension
    try {
      fs.writeFileSync(
        'static/assets/upload/pictures/' + fileName,
        imageBuffer,
        'utf8'
      )
      let data = {
        name: date_n,
        type: extension,
        size: 1000,
        path: '/assets/upload/pictures/' + fileName
      }
      db.files
        .create(data)
        .then(file => {
          let f = file
          res.json(f)
        })
        .catch(err =>
          res.json({
            code: 'error_create',
            message: 'El archivo no pudo ser subido al servido'
          })
        )
    } catch (err) {
      res.send(500).json({
        error: true,
        message: 'Hubo un problema para subir la imagen'
      })
      console.error(err)
    }
  })

  function decodeBase64Image(dataString) {
    var matches = dataString.match(/^data:([A-Za-z-+\/]+);base64,(.+)$/),
      response = {}

    if (matches.length !== 3) {
      return new Error('Invalid input string')
    }

    response.type = matches[1]
    response.data = new Buffer.from(matches[2], 'base64')

    return response
  }

  function randomString(length) {
    var chars = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz'
    var string_length = length
    var randomstring = ''
    for (var i = 0; i < string_length; i++) {
      var rnum = Math.floor(Math.random() * chars.length)
      randomstring += chars.substring(rnum, rnum + 1)
    }
    return randomstring
  }

  return app
})()
