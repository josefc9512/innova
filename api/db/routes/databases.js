// databases.js
module.exports = (function() {
  'use strict'
  const app = require('express').Router()
  const db = require('../config/config')
  require('dotenv').config()

  app.post('/', (req, res) => {
    const body = req.body

    db.databases
      .create(body)
      .then(database => {
        res.json(database)
      })
      .catch(err => {
        res.json(err)
      })
  })

  app.get('/', (req, res) => {
    db.databases
      .findAll({
        where: {
          status: 1
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(databases => {
        res.json(databases)
      })
  })

  app.get('/:id', (req, res) => {
    const id = req.params.id
    if (!id)
      return res.status(404).json({
        error: true,
        message: 'No se puede realizar la consulta por falta de datos'
      })

    db.databases
      .findOne({
        where: {
          id: id
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(databases => {
        res.json(databases)
      })
  })

  app.put('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.databases
      .update(update, {
        where: {
          id: id
        }
      })
      .then(project => {
        res.json(project)
      })
  })

  app.delete('/:id', (req, res) => {
    const id = req.params.id
    db.databases
      .update(
        {
          status: 0
        },
        {
          where: {
            id: id
          }
        }
      )
      .then(project => {
        res.json(project)
      })
  })

  return app
})()
