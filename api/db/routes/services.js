// services.js
module.exports = (function() {
  'use strict'
  const app = require('express').Router()
  const db = require('../config/config')
  require('dotenv').config()

  app.post('/', (req, res) => {
    const body = req.body

    db.services
      .create(body)
      .then(search => {
        res.json(search)
      })
      .catch(err => {
        res.json(err)
      })
  })

  app.get('/', (req, res) => {
    db.services
      .findAll({
        where: {
          status: 1
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(services => {
        res.json(services)
      })
  })

  app.get('/:id', (req, res) => {
    const id = req.params.id
    if (!id)
      return res.status(404).json({
        error: true,
        message: 'No se puede realizar la consulta por falta de datos'
      })

    db.services
      .findOne({
        where: {
          id: id
        },
        include: [
          {
            model: db.files,
            require: false
          }
        ]
      })
      .then(services => {
        res.json(services)
      })
  })

  app.put('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.services
      .update(update, {
        where: {
          id: id
        }
      })
      .then(project => {
        res.json(project)
      })
  })

  app.delete('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.services
      .update(
        {
          status: 0
        },
        {
          where: {
            id: id
          }
        }
      )
      .then(project => {
        res.json(project)
      })
  })

  return app
})()
