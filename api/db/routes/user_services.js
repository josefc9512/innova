// user_services.js
module.exports = (function () {
  'use strict'
  const app = require('express').Router()
  const db = require('../config/config')
  require('dotenv').config()

  app.post('/', (req, res) => {
    const body = req.body
    db.user_services
      .create(body)
      .then(response => {
        res.json(response)
      })
      .catch(err => {
        res.json(err)
      })
  })

  app.get('/', (req, res) => {
    db.user_services
      .findAll({
        where: {
          status: 1
        },
        include: [
          {
            model: db.users,
            require: false
          },
          {
            model: db.services,
            require: false
          }
        ]
      })
      .then(user_services => {
        res.json(user_services)
      })
  })

  app.get('/:id', (req, res) => {
    const id = req.params.id
    if (!id)
      return res.status(404).json({
        error: true,
        message: 'No se puede realizar la consulta por falta de datos'
      })

    db.user_services
      .findOne({
        where: {
          id: id
        },
        include: [
          {
            model: db.users,
            require: false
          },
          {
            model: db.services,
            require: false
          }
        ]
      })
      .then(user_services => {
        res.json(user_services)
      })
  })

  app.put('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body

    upsert(update, { id: id }).then(function (result) {
      res.status(200).json(result)
    });

    // db.user_services
    //   .update(update, {
    //     where: {
    //       id: id
    //     }
    //   })
    //   .then(user_services => {
    //     res.json(user_services)
    //   })
  })

  app.delete('/:id', (req, res) => {
    const id = req.params.id
    db.user_services
      .update(
        {
          status: 0
        },
        {
          where: {
            id: id
          }
        }
      )
      .then(user_services => {
        res.json(user_services)
      })
  })

  function upsert(values, condition) {
    return db.user_services
      .findOne({ where: condition })
      .then(function (obj) {
        // update
        if (obj) {
          return obj.update(values);
        }
        else {
          // insert
          delete values.id
          return db.user_services.create(values);
        }
      })
  }

  return app
})()
