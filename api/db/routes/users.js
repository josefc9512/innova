// Users.js
module.exports = (function () {
  'use strict'
  const app = require('express').Router()
  const db = require('../config/config')
  const bcrypt = require('bcrypt')
  require('dotenv').config()
  const jwt = require('express-jwt')
  const jsonwebtoken = require('jsonwebtoken')

  var nodemailer = require('nodemailer')
  const TOKEN_ROUTE = process.env.TOKEN_ROUTE

  const checkToken = (req, res, next) => {
    const header = req.headers['authorization']

    if (typeof header !== 'undefined') {
      const bearer = header.split(' ')
      const token = bearer[1]

      req.token = token
      next()
    } else {
      res.sendStatus(403)
    }
  }

  app.post('/', (req, res) => {
    const body = req.body

    const { names, password, name, email } = body

    let password_hash = bcrypt.hashSync(password, 10)

    db.users
      .create({
        names: names,
        name: name,
        password: password_hash,
        email: email
      })
      .then(user => {
        res.json(user)
      })
      .catch(err => {
        res.json(err)
      })
  })

  app.post('/login', async (req, res) => {
    if (Object.keys(req.body).length != 0) {
      const body = req.body
      const { email, password } = body

      if (email && password) {
        let countUser = await ifExistUser(email).then(count => {
          return count
        })

        if (countUser > 0) {
          let user = await getUser(email).then(result => {
            return result.dataValues
          })

          console.log(user)

          var passwordDB = user.password

          if (bcrypt.compareSync(password, passwordDB)) {
            if (user.role != 'admin') {
              res.status(403).json({
                error: true,
                message: 'Usted no tiene los permisos necesarios para ingresar'
              })
            }
            res.status(200).json({
              status: 200,
              message: 'Bienvenido',
              user: user
            })
          } else {
            res.status(203).json({
              status: 203,
              message: 'La contraseña no coincide'
            })
          }
        } else {
          res.status(203).json({
            status: 203,
            message: 'El usuario no existe'
          })
        }
      } else {
        res.status(203).json({
          status: 203,
          message: 'Hubo un error a la hora de realizar tu consulta'
        })
      }
    } else {
      res.status(203).json({
        status: 203,
        message: 'Hay campos vacios'
      })
    }
  })

  app.post('/register', async (req, res) => {
    const body = req.body

    const { names, password, email, role } = body

    let countUser = await ifExistUser(email).then(count => {
      return count
    })
    let password_hash = bcrypt.hashSync(password, 10)

    body.password = password_hash

    if (!countUser) {
      db.users
        .create(body)
        .then(async user => {
          res.json(user)
        })
        .catch(err => {
          res.json(err)
        })
    } else {
      res.status(203).json({
        status: 203,
        message: 'Este correo ya esta registrado'
      })
    }
  })

  app.get('/', (req, res) => {
    db.users
      .findAll({
        where: {
          status: 1
        },
        attributes: [
          'id',
          'names',
          'email',
          'phone',
          'role',
          'type_login',
          'status'
        ]
      })
      .then(users => {
        res.json(users)
      })
  })

  app.get('/:id', (req, res) => {
    const id = req.params.id
    if (!id)
      return res.status(404).json({
        error: true,
        message: 'No se puede realizar la consulta por falta de datos'
      })

    db.users
      .findOne({
        where: {
          id: id,
          status: 1
        },
        attributes: [
          'id',
          'names',
          'email',
          'phone',
          'role',
          'type_login',
          'status'
        ],
        include: [
          {
            model: db.user_services,
            required: false,
            where: {
              status: 1
            }
          }
        ]
      })
      .then(users => {
        res.json(users)
      })
  })

  app.get('/user/data', (req, res) => {
    const id = req.params.id
    db.users
      .findOne({
        where: {
          id: id
        },
        attributes: [
          'id',
          'names',
          'email',
          'phone',
          'role',
          'type_login',
          'status'
        ]
      })
      .then(users => {
        res.json(users)
      })
  })

  app.put('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.users
      .update(update, {
        where: {
          id: id
        }
      })
      .then(project => {
        res.json(project)
      })
  })

  app.delete('/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    db.users
      .update(
        {
          status: 0
        },
        {
          where: {
            id: id
          }
        }
      )
      .then(user => {
        res.json(user)
      })
  })

  app.put('/resetpass/:id', (req, res) => {
    const id = req.params.id
    let update = req.body
    const { password, email } = update

    let password_hash = bcrypt.hashSync(password, 10)

    update.password = password_hash

    db.users
      .update(update, {
        where: {
          id: id
        }
      })
      .then(user => {
        let message =
          'Estimado usuario, su contraseña se ha reestablecido, su nueva contraseña es: ' +
          password
        sendEmailCustomMessage(
          email,
          'Su contraseña se ha reestablecido',
          message
        )
        res.json(user)
      })
  })

  app.post('/resetpassapp', async (req, res) => {

    const body = req.body

    const { email } = body

    let user = await getUser(email).then(result => {
      return result
    })

    if (!user) {
      res.json({
        error: false,
        _message: 'El usuario no existe'
      })
    }

    let new_password = generatePassword()

    let password_hash = bcrypt.hashSync(new_password, 10)

    let update = {
      password: password_hash
    }

    db.users
      .update(update, {
        where: {
          email: email
        }
      })
      .then(user => {
        let message =
          'Estimado usuario, su contraseña se ha reestablecido, su nueva contraseña es: ' +
          new_password
        sendEmailCustomMessage(
          email,
          'Su contraseña se ha reestablecido',
          message
        )
        res.json({
          error: false,
          _message: 'Se reestablecio correctamente tu contraseña, y se ha enviado a tu correo electronico'
        })
      })
  })

  function generatePassword() {
    var length = 8,
      charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
      retVal = "";
    for (var i = 0, n = charset.length; i < length; ++i) {
      retVal += charset.charAt(Math.floor(Math.random() * n));
    }
    return retVal;
  }

  var transporter = nodemailer.createTransport({
    host: 'hd-5110.banahosting.com',
    port: 465,
    secure: true, // use TLS
    auth: {
      user: 'no-reply@innova.somossistemas.pe',
      pass: '-Rt-nM$&f!Sg'
    }
  })

  app.post('/sendEmail', (req, res) => {
    const body = req.body

    if (!body)
      res.send(505).json({
        error: true,
        message: 'Se requieren datos de envio para poder enviar un correo'
      })

    var mailOptions = {
      from: 'no-reply@innova.somossistemas.pe',
      to: 'contacto@innovascientific.com',
      subject: 'La persona: ' + body.name,
      text:
        'Tiene un mensaje para ti: ' +
        body.affair +
        ' <br> Y su telefono es: ' +
        body.phone +
        ' <br> Y su correo es : ' +
        body.email
    }

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error)
      } else {
        console.log('Email sent: ' + info.response)
        res.json({
          error: false,
          message: 'El correo se ha enviado'
        })
      }
    })
  })

  app.put('/update/photo', (req, res) => {
    const body = req.body
    if (!body)
      res.send(400).json({
        error: true,
        message: 'Se requieren datos de envio para poder enviar un correo'
      })

    if (!body.picture)
      res.send(400).json({
        error: true,
        message: 'No puedes enviar la imagen vacía'
      })

    let picture = body.picture
    const id = body.id

    let update = {
      picture: picture.id
    }
    db.users
      .update(update, {
        where: {
          id: id
        }
      })
      .then(user => {
        res.json(user)
      })
  })

  function verifyToken(token) {
    return jsonwebtoken.verify(token, TOKEN_ROUTE, (err, dataClean) => {
      if (err) {
        return false
      } else {
        return dataClean
      }
    })
  }

  function sendEmailCustomMessage(email, subject, message) {
    var mailOptions = {
      from: 'no-reply@innova.somossistemas.pe',
      to: email,
      subject: subject,
      text: message
    }

    transporter.sendMail(mailOptions, function (error, info) {
      if (error) {
        console.log(error)
      } else {
        console.log('Email sent: ' + info.response)
      }
    })
  }

  function getUser(email) {
    return db.users.findOne({
      where: {
        email: email
      }
    })
  }

  function ifExistUser(email) {
    return db.users.count({
      where: {
        email: email
      }
    })
  }

  return app
})()
