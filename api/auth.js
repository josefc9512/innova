const express = require('express')
const bodyParser = require('body-parser')
const cookieParser = require('cookie-parser')
const jwt = require('express-jwt')
const jsonwebtoken = require('jsonwebtoken')

require("dotenv").config();

const TOKEN_ROUTE = process.env.TOKEN_ROUTE
// Create app
const app = express()
app.disable('x-powered-by')
// Install middleware
app.use(cookieParser())
app.use(bodyParser.json())

// JWT middleware
app.use(
  jwt({
    secret: TOKEN_ROUTE
  }).unless({
    path: '/auth/login'
  })
)

// -- Routes --

// [POST] /login
app.post('/login', (req, res, next) => {
  const { user } = req.body
  const valid = user

  if (!valid) {
    // throw new Error('Invalid username or password')
    res.send(401, 'Error al logearse')
  }

  const accessToken = jsonwebtoken.sign(
    {
      id: user.id,
      names: user.names,
      email: user.email,
      phone: user.phone,
    },
    TOKEN_ROUTE
  )
  res.json({
    token: {
      accessToken
    }
  })
})

// [GET] /user
app.get('/user', (req, res, next) => {
  res.json({ user: req.user })
})

// [POST] /logout
app.post('/logout', (req, res, next) => {
  res.json({ status: 'OK' })
})

// Error handler
app.use((err, req, res, next) => {
  console.error(err) // eslint-disable-line no-console
  res.status(401).send(err + 'a')
})

// -- export app --
module.exports = {
  path: '/auth',
  handler: app
}
